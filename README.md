# SFRA


```
.
├── .editorconfig
├── .git{config,ignore,modules}
├── .{npm,yarn}rc
├── package.json
├── README.md
├── sfra.code-workspace
│
├── brand.com
│   ├── package.json
│   └── cartridges
│       ├── app_core_brand
│       ├── app_custom_jackjones
│       ├── app_custom_only
│       ├── app_custom_veromoda
│       └── ...
│
├── bestseller.com
│   ├── package.json
│   └── cartridges
│       ├── app_core_bestseller
│       └── app_custom_bestseller
│
├── partner.com
│   ├── package.json
│   └── cartridges
│       ├── app_core_partner
│       ├── app_custom_zalando
│       ├── app_custom_aboutyou
│       └── ...
│
├── storefront-reference-architecture
│   ├── package.json
│   └── cartridges
│       ├── modules
│       ├── app_storefront_base
│       └── bm_app_storefront_base
│
└── site_configuration
    ├── instance_config
    │   ├── dev01-...
    │   └── ...
    └── site_template
        ├── version.txt
        └── ...
```
